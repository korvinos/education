#include <stdio.h>

/* Hello, world! */

int hello() {
    printf("Hello, world!\n");
    return 0;
}

/* Data types */
/*
 * Integers: whole numbers which can be positive and negative:
 *  - char, int, short, long, long long
 * Unsigned integers: whole numbers which can only be positive:
 *  - unsigned char, unsigned int, ... short, ... long, ... long long
 * Floating point: real numbers:
 *  - float, double
 * Structures
 */

/* Define the bollian type (is not included */

#define BOOL char
#define FALSE 0
#define TRUE 1

int vardefining() {
    int foo;
    int bar = 12;
    printf("I will go to the bar at %d\n", bar);
    int result = bar + 8; 
    printf("%d\n", result);
    return result;
}

/* Arrays */

int arr() {
    /* Define array 0:9 */
    int numbers[10];
    printf("The 7th number in the array is %d\n", numbers[6]);
    return 0;
}

/* Main function */
int main() {
    hello();
    vardefining();
    arr();
    return 0;
}


